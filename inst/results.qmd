---
always_allow_html: true
---

# Results {#sec-results}

```{r}
#| echo: false
#| output: false

devtools::load_all()
library(tidyverse)
```

All `data/*.rda` were loaded with `devtools::load_all()`.

::: {#fig-data-folder}
```{bash}
#| echo: false

ls -1 ../data
```

Content of folder `data`.
:::

## Tables

```{r}
#| label: tbl-iris
#| tbl-cap: Edgar Anderson's Iris Data.

edgar.data |>
  group_by(Species) |>
  summarise(
    Sepal.Length = mean(Sepal.Length),
    Sepal.Width = mean(Sepal.Width),
    Petal.Length = mean(Petal.Length),
    Petal.Width = mean(Petal.Width)
  ) |>
  ungroup()
```

## Data Visualisation {#sec-vis}

::: callout-important
This file needs `always_allow_html: true` in the file YAML header (not `_quarto.yml`).
:::

```{r}
#| echo: true
#| warning: false
#| error: false
#| output: false

library(ggplot2)
library(plotly)
```

```{r}
#| echo: true
#| warning: false
#| error: false
#| output: false

p <- edgar.data |>
  ggplot(aes(
    x = Petal.Length,
    y = Petal.Width
  )) +
  geom_point()
```

::: {#fig-iris}
::: {.content-visible unless-format="html"}
```{r}
#| echo: false
#| warning: false
#| error: false
#| output: true

p
```
:::

::: {.content-visible when-format="html"}
```{r}
#| echo: true
#| warning: false
#| error: false
#| output: true

p |>
  ggplotly() |>
  layout(dragmode = "pan")
```
:::

Chart using Edgar Anderson's Iris Data.
:::
